//
//  PokemonInfoViewController.swift
//  Pokedex-2018
//
//  Created by alexolmedo on 20/6/18.
//  Copyright © 2018 Alexander Olmedo. All rights reserved.
//

import UIKit

class PokemonInfoViewController: UIViewController {
    
    var itemInfo:(pokemon:[Pokemon], index: Int)?

    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var heightLabel: UILabel!
    
    @IBOutlet weak var weightLabel: UILabel!
    
    
    @IBOutlet weak var pokemonImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel.text=itemInfo?.pokemon[(itemInfo?.index)!].name
        
        if let v = itemInfo?.pokemon[(itemInfo?.index)!].height {
            heightLabel.text = "\(v)"
        }
        
        if let v = itemInfo?.pokemon[(itemInfo?.index)!].weight {
            weightLabel.text = "\(v)"
        }
        
        
        let bm = Network()
        
        bm.getImage((itemInfo?.pokemon[(itemInfo?.index)!].sprites.defaultSprite)!, completionHandler: { (imageR) in
            
            DispatchQueue.main.async {
                self.pokemonImage.image = imageR
            }
            
        })
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
